FROM registry.gitlab.com/wownikcompany/docker/image_node:latest

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

COPY . /usr/src/app/
RUN yarn install

CMD ["yarn", "start"]
