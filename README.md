# shikicinema

Это скомунизженный код из расширения [shikicinema](https://chromewebstore.google.com/detail/shikicinema/hmbjohbggdnlpmokjbholpgegcdbehjp)

Было взято расширение для браузера v0.8.23, и нагло перенесено в уже скомпилированном виде, в static папку.
Быстро поднят сервер на express, тупо отдающий эти файлы. Подкорректирован main.js, чтобы код расширения не конфликтовал с web версией.

Что там в действительности под капотом - шут его знает
