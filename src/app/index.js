import express from 'express';
import { fileURLToPath } from 'url';
import path, { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export function appStart() {
  const app = express();
  const port = 3000;

  app.get('/', (req, res) => {
    res.redirect("/index.html#/animeId/1");
  });

  app.get('/:filename', (req, res) => {
    const {filename} = req.params;

    const filePath = path.join(__dirname, '../../static', filename);
    res.sendFile(filePath);
  });

  app.listen(port, () => {
    console.log(`shikicinema app listening on port ${port}`);
  });
}
